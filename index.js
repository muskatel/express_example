const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const morgan = require('morgan')

const app = express()
const port = process.env.PORT || 8080

app.use(morgan('dev'))

app.use(bodyParser.json())

let data; //null

// GET / 
// index.html
app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, 'webpages/index.html'))
})


// POST /data 
app.post('/data', (req, res) => {

	if(!data)
	{
		data = req.body
	}
	res.status(201).send(data)
})

// GET /data 
app.get('/data', (req, res) => {
	res.send(data)
})

// PUT /data 
app.put('/data', (req, res) => {
	data = req.body
	res.status(201).send(data)
}) 

// DELETE /data 
app.delete('/data', (req, res) => {
	data = null
	
	res.status(200).send("Data was deleted")
})

app.listen(port, () => 
	console.log("Server has started on port", port))
